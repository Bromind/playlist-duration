pub fn features() -> String {
    let mut features = String::new();
    if cfg!(feature = "mp3") {
        features += "+mp3 ";
    } else {
        features += "-mp3 ";
    }
    if cfg!(feature = "flac") {
        features += "+flac ";
    } else {
        features += "-flac ";
    }
    if cfg!(feature = "ogg") {
        features += "+ogg ";
    } else {
        features += "-ogg ";
    }
    if cfg!(feature = "m3u") {
        features += "+m3u ";
    } else {
        features += "-m3u ";
    }
    if cfg!(feature = "m4a") {
        features += "+m4a ";
    } else {
        features += "-m4a ";
    }
    features
}
