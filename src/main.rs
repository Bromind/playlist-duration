use std::path::{Path, PathBuf};
extern crate mime_guess;
use mime_guess::from_path;
extern crate mime;
#[allow(unused_imports)] // SubLevel is unused if no filetype is supported
use mime::Mime;
use std::time::Duration;
extern crate clap;
use clap::{Arg, Command};

#[cfg(feature = "mp3")]
extern crate mp3_duration;

#[cfg(feature = "flac")]
extern crate metaflac;
#[cfg(feature = "flac")]
use metaflac::Tag;

#[cfg(feature = "ogg")]
extern crate ogg_metadata;
#[cfg(feature = "ogg")]
use ogg_metadata::read_format;
#[cfg(feature = "ogg")]
use ogg_metadata::AudioMetadata;
#[cfg(feature = "ogg")]
use ogg_metadata::OggFormat::{Opus, Vorbis};
#[cfg(feature = "ogg")]
use std::fs::File;

#[cfg(feature = "m4a")]
use mp4ameta::Tag as M4ATag;

#[cfg(feature = "mp3")]
fn mpeg(p: &Path) -> Option<Duration> {
    mp3_duration::from_path(p).ok()
}

mod features;
use features::features;

#[cfg(feature = "m4a")]
fn m4a(p: &Path) -> Option<Duration> {
    let tag = match M4ATag::read_from_path(p) {
        Ok(t) => t,
        Err(e) => {
            println!(
                "Unable to read from path {}: {}",
                p.to_str().unwrap_or("unknown path"),
                e
            );
            return None;
        }
    };
    tag.duration()
}

#[cfg(feature = "flac")]
fn flac(p: &Path) -> Option<Duration> {
    let tag = match Tag::read_from_path(p) {
        Ok(t) => t,
        Err(e) => {
            println!(
                "Unable to read from path {}: {}",
                p.to_str().unwrap_or("unknown path"),
                e
            );
            return None;
        }
    };
    let stream_info = tag.get_streaminfo()?;
    let nb_sec = stream_info.total_samples / stream_info.sample_rate as u64;
    return Some(Duration::from_secs(nb_sec));
}

#[cfg(feature = "ogg")]
fn ogg(p: &Path) -> Option<Duration> {
    let file = match File::open(p) {
        Ok(f) => f,
        Err(e) => {
            println!(
                "Can not open file {}: {}",
                p.to_str().unwrap_or("unknown path"),
                e
            );
            return None;
        }
    };
    match read_format(file) {
        Ok(v) => match v[0] {
            Vorbis(ref m) => m.get_duration(),
            Opus(ref m) => m.get_duration(),
            _ => None,
        },
        Err(e) => {
            println!(
                "Unable to read from path {}: {}",
                p.to_str().unwrap_or("unknown path"),
                e
            );
            None
        }
    }
}

#[cfg(feature = "m3u")]
fn m3u(p: &Path) -> Option<Duration> {
    let mut r = match m3u::Reader::open(p) {
        Ok(r) => r,
        Err(_) => return None,
    };
    let paths = r
        .entries()
        .filter(|r| match r {
            Ok(e) => e.is_path(),
            Err(_) => false,
        })
        .map(|e| match e {
            Ok(m3u::Entry::Path(pe)) => {
                let mut complete_path = PathBuf::from(p);
                complete_path.pop();
                complete_path.push(pe);
                complete_path
            }
            _ => panic!("Url found after filter"),
        });
    Some(get_total_time(paths))
}

fn directory(p: &Path) -> Option<Duration> {
    p.read_dir() // Result<ReadDir, Error>
        .map(|rd| // rd: ReadDir: Iterator<Item = Result<DirEntry>>
             rd.filter(Result::is_ok)
                .map(|e| e // Result<DirEntry>
                     .unwrap() // DirEntry
                     .path() // PathBuf
                )) // Result<Iterator<Item = PathBuf>, Error>
        .map(|paths| get_total_time(paths))
        .ok()
}

fn get_total_time<A>(i: A) -> Duration
where
    A: Iterator<Item = PathBuf>,
{
    let mut total_time = Duration::new(0, 0);
    for a in i {
        let p = Path::new(&a);
        let is_dir = p.symlink_metadata().map(|m| m.is_dir()).unwrap_or(false);
        if is_dir {
            total_time += directory(p).unwrap_or(Duration::from_secs(0));
        } else {
            let guess = from_path(&p);
            match guess.first() {
                Some(mime) => match mime.type_() {
                    mime::AUDIO => match mime.subtype() {
                        #[cfg(feature = "mp3")]
                        mime::MPEG => {
                            total_time += mpeg(&p).unwrap_or(Duration::from_secs(0));
                        }
                        #[cfg(feature = "ogg")]
                        mime::OGG => {
                            total_time += ogg(&p).unwrap_or(Duration::from_secs(0));
                        }
                        #[cfg(feature = "flac")]
                        name if name == "flac" => {
                            total_time += flac(&p).unwrap_or(Duration::from_secs(0));
                        }
                        #[cfg(feature = "m3u")]
                        name if name == "x-mpegurl" => {
                            total_time += m3u(&p).unwrap_or(Duration::from_secs(0));
                        }
                        #[cfg(feature = "m4a")]
                        name if name == "m4a" => {
                            total_time += m4a(&p).unwrap_or(Duration::from_secs(0));
                        }
                        _ => {
                            println!(
                                "MimeType {}/{} (file {}) is not supported... Ignored.",
                                mime::AUDIO,
                                mime.subtype(),
                                p.display()
                            );
                        }
                    },
                    _ => {
                        println!(
                            "MimeType {}/{} is not an audio format... Ignored.",
                            mime.type_(),
                            mime.subtype()
                        );
                    }
                },
                _ => println!(
                    "The MimeType of the file {} could not be recognized",
                    p.display()
                ),
            }
        }
    }
    total_time
}

fn main() {
    let matches = Command::new("playlist-duration")
        .author("Martin Vassor <martin@vassor.org>")
        .version("0.1.9")
        .arg(
            Arg::new("features")
                .long("features")
                .short('f')
                .help("Prints features enabled"),
        )
        .arg(
            Arg::new("FILES")
                .multiple_occurrences(true)
                .takes_value(true)
                .help("The files of the playlist")
                .required_unless_present_any(&["features"]),
        )
        .get_matches();
    if matches.is_present("features") {
        println!("Features: {}", features());
    } else {
        let files = matches
            .values_of("FILES")
            .unwrap()
            .map(|e| PathBuf::from(e));
        let total_time = get_total_time(files);
        println!(
            "Total duration of the playlist : {}",
            duration_to_string(&total_time)
        );
    }
}

fn duration_to_string(d: &Duration) -> String {
    let nanos = d.subsec_nanos();
    let whole_seconds = d.as_secs();
    let whole_minutes = whole_seconds / 60;
    let whole_hours = whole_minutes / 60;
    let whole_days = whole_hours / 24;

    let hours = whole_hours % 24;
    let minutes = whole_minutes % 60;
    let seconds = whole_seconds % 60;

    format!(
        "{} days, {} hours, {} minutes and {}.{} seconds.",
        whole_days, hours, minutes, seconds, nanos
    )
}
