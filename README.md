Playlist-Duration
=================

Computes the length of a given playlist.

```
$ playlist-duration foo.mp3 bar.flac my_directory/
```

Format supported:
- mp3
- flac
- ogg (Vorbis + Opus)
- m3u : Caution, m3u is implemented naively. Recursive m3u playlists should not be used. Also, it manages only paths and not urls, to avoid downloading the whole file just to know its duration.
- m4a

